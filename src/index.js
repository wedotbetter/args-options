import _ from 'lodash';

export const parseArguments = (argKeys, args) => {
    let lastArg = args[args.length-1];
    let options = _.isPlainObject(lastArg) ? args.pop() : {};

    _.forEach(argKeys, (keys, i) => {
        keys = _.isArray(keys) ? keys : [keys];
        if (args[i]) {
            _.forEach(keys, key => {
                _.set(options, key, args[i])
            });
        }
    });

    return options;
}

export const argOptions = argKeys => fn => {
    return function(...args) {
        let options = parseArguments(argKeys, args);
        return fn.call(this, options);
    }
}

export default argKeys => {
    return function(proto, key, descriptor) {

        if (descriptor.value) {
            descriptor.value = argOptions(argKeys)(descriptor.value);
        }

        if (descriptor.initializer) {
            let origInitializer = descriptor.initializer;
            descriptor.initializer = function() {
                return argOptions(argKeys)(origInitializer.call(this));
            }
        }

        return descriptor;
    }
}
