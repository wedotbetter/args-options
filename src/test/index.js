import argOptionsDecorator,  { argOptions, parseArguments } from '../index';
import assert from 'assert';

describe('args-options', function() {
    const argKeys = [ 'a', ['e', 'f'], 'b.c', 'b.d' ];
    let fn = function(options) {
        return options;
    }
    class Test {
        constructor(...args) {
            let options = parseArguments(argKeys, args);
            this.options = options;
        }

        @argOptionsDecorator(argKeys)
        functionAsValue(options) {
            let testContext = this.options;
            return {
                options,
                testContext
            };
        }
        @argOptionsDecorator(argKeys)
        functionAsInitializer = (options) => {
            let testContext = this.options;
            return {
                options,
                testContext
            };
        }
    }
    const test = [1, [ 4, 5, 6 ], 2, { b : { d : 3 } }];
    const tester = new Test(...test);
    const testResult = { a : 1, b : { c : 2, d : 3 }, e : [ 4, 5, 6 ], f : [ 4, 5, 6 ] };
    it('should decorate pure function', function() {
        assert.deepEqual(argOptions(argKeys)(fn)(...test), testResult);
    });
    it('should decorate class method (as value) and preserve context', function() {
        let result = tester.functionAsValue(...test)
        assert.deepEqual(result.options, testResult);
        assert.deepEqual(result.testContext, testResult);
    });
    it('should decorate class method (as initializer) and preserve context', function() {
        let result = tester.functionAsInitializer(...test)
        assert.deepEqual(result.options, testResult);
        assert.deepEqual(result.testContext, testResult);
    });
    it('should decorate constructor', function() {
        assert.deepEqual(tester.options, testResult);
    });
})
